var express = require('express');
var router = express.Router();
var db = require('mongoose');
db.connect('mongodb://localhost/usertest');


var schema = new db.Schema({
id    : { type: String }
,name  : { type: String }
,age   : { type: Number }
});


exports.create = function(req, res){
		User.create({
id   : req.param('id')
,name : req.param('name')
,age  : req.param('age')
}, function(err) {
res.send('created')
});
};

var User = db.model('User', schema);

exports.list = function(req, res){
		User.find({}, function(err, users) {
						res.render('users', {
users: users
});
						})
};

/* GET users listing. */
router.get('/', function(req, res, next) {
				res.send('respond with a resource');
				});

module.exports = router;
